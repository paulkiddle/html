# Changelog

## [3.1.0]

Adds `attrs` function for generating elemnet attributes.

## [3.0.3]

Adds `type='text/html'` to Html objects.

## [3.0.0](https://gitlab.com/paulkiddle/html/compare/v2.0.1...v3.0.0) (2023-12-01)

Completeley rewritten and changed the API.

Now includes typescript definitions.

### ⚠ BREAKING CHANGES

 - Promises nor async iterators are no longer supported
 - `html` does not return a promise
 - The symbol-based protocols are no longer supported
 - Please see the documentation for the new API.


### [2.0.1](https://gitlab.com/paulkiddle/html/compare/v2.0.0...v2.0.1) (2022-07-30)


### Bug Fixes

* Handle streaming sync iterator that contains async stream ([16ff647](https://gitlab.com/paulkiddle/html/commit/16ff647a42ba57dbd4a486a7732423b7830d6ffa))

## [2.0.0](https://gitlab.com/paulkiddle/html/compare/v1.0.1...v2.0.0) (2022-06-08)


### ⚠ BREAKING CHANGES

* Change exports to reduce complexity

 - `html` is no longer a named export, please use the default export
 - `html.sync`/`sync` has been merged with `html`, use `html` for both sync and async templates
 - `[html.]sync.stringify` has been renamed to `stringifySync` and `html.stringifySync`
 - `html.stream` is now also exported as named export `stream`


### Features

* Change exports to reduce complexity ([21dff10](https://gitlab.com/paulkiddle/html/commit/21dff1084b50c785f18a922d10e2494ee09771be))

### [1.0.1](https://gitlab.com/paulkiddle/html/compare/v1.0.0...v1.0.1) (2022-05-05)


### Bug Fixes

* Change chunk encoder to check for toString instead of source ([3515ebf](https://gitlab.com/paulkiddle/html/commit/3515ebff7934ad43fc96edcc54211bb6dcc3d88f))
