const ENTITIES: Record<string, string> = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	'\'': '&#39;',
	'/': '&#x2F;',
	'`': '&#x60;',
	'=': '&#x3D;'
};
const ENT_REGEX = new RegExp(Object.keys(ENTITIES).join('|'), 'g');
/**
 * @private
 * Encodes a string to be html-safe by turning certain characters into their html encoded counterpart
 * @param {String} str The string to replace html entities for
 * @returns {String}
 */
const encodeHtml = (str: string) => str.replace(ENT_REGEX, char => ENTITIES[char]);

export default encodeHtml;
