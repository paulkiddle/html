import encodeHtml from "./encode-html.js";

export interface HtmlObject {
	type?: 'text/html'
	value: string
}
type Html = Required<HtmlObject>

export type HtmlTemplateVariable = HtmlObject | string | number | undefined | null | Iterable<HtmlTemplateVariable>;

const isObject = (value: unknown): value is object => (typeof value === 'object') && !!value;
const objectIsHtml = (value: object): value is HtmlObject => ('value' in value) && (!('type' in value) || (value.type === 'text/html'))

function *getEncodedStrings(val: HtmlTemplateVariable): Iterable<string> {
	if(typeof val === 'string') {
		yield encodeHtml(val);
		return;
	}

	if(typeof val === 'number') {
		yield val.toString();
		return;
	}

	if(!val) {
		return;
	}

	if('value' in val){
		yield val.value;
		return;
	}

	if(Symbol.iterator in val) {
		for(const item of val) {
			yield* getEncodedStrings(item);
		}
		return;
	}


	throw new Error(`Value ${val} is not supported as an HTML template variable.`);
}

class HtmlInstance implements Html {
	#value

	constructor(value: string) {
		this.#value = value;
	}

	get value(){
		return this.#value
	}

	get type() {
		return 'text/html' as const;
	}

	toString(){
		return this.value;
	}

	/**
	 * Template function for tagging html strings
	 * @param lits Literal html values
	 * @param vars String value for encoding or HTML object
	 * @returns 
	 */
	static html({ raw: [lit0, ...lits] }: TemplateStringsArray, ...vars: HtmlTemplateVariable[]) {
		return new Html(lits.reduce((value, lit, ix) => {
			return value + Html.encode(vars[ix]) + lit;
		}, lit0));
	}

	/**
	 * Turn an HTML object or other value into a string with correct entity encoding
	 * @param variable String value for encoding or HTML object
	 */
	static encode(variable: HtmlTemplateVariable) {
		let value = '';
			
		for(const chunk of getEncodedStrings(variable)) {
			value += chunk;
		}
	
		return value;
	}
	
	static isHtml(value: unknown): value is HtmlObject {
		return isObject(value) && objectIsHtml(value)
	}

	/**
	 * Returns an array of html objects representing html attribute pairs.
	 * Each attribute is prefixed by a space.
	 * Attributes whose value is false or null will be omitted
	 * Attributes whose value is true will only have their name output
	 * @param attrs Dict of attribute keys and values
	 * @returns {Html[]} Array of HTML objects representing attributes
	 */
	static attrs(attrs: Record<string, string|boolean|null>) {
		const parts: Html[] = [];
	
		for(const [k, v] of Object.entries(attrs)) {
			if(v !== false && v != null) {
				const value = v === true ? null : html`="${v}"`
				parts.push(html` ${k}${value}`)
			}
		}

		return parts;
	}
}

const Html = HtmlInstance;

export { Html };

export const { html, encode, isHtml, attrs } = Html;

export default Object.assign(html, { encode, isHtml, attrs });
