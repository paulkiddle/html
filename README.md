# Html Types

Provides a number of small tools for working with HTML:

1. Protocol definitions for objects that contain html data (or streams of html data)
2. Simple classes that implement those protocols
3. Template tag functions for generating html, with nesting
	and encoding of string data built in.
4. Functions for generating safe HTML from javascript values


```typescript
import html, { Html, isHtml, encode } from 'html-types';
import { type HtmlObject, type HtmlTemplateVariable } from 'html-types';

// 1. Protocols & typedefs:
const posts = [{
	title: 'My blog'
	body: {
		value: '<b>Welcome</b> to my blog!'
	}
},{
	title: 'I <3 my dog'
	body: {
		value: 'I really love my dog!'
	} as HtmlObject
}]

// 2. Classes & utilities:
const author = new Html('By <a href="http://paul.example/">Paul</a>');
isHtml(author) // True

// 3. Template tags:
const page = html`
	<h1>Paul's Blog</h1>
	${posts.map(post=>html`
		<h2>${post.title}</h2>
		<i>${author}</i>
		${post.body}
	`)}
`;

// 4. Source stringifiers:
console.log(encode(page));

```

## More details

This library helps you work with HTML in a type-safe way, by
creating a differentiation between HTML and non-html strings.

To make use of it, you just need to call one of the stringify functions
whenever you need to output HTML:

```javascript
import html from 'html-types';
import fs from 'fs';

console.log(html.encode('Solve the equation 4x + y > 5'));
// Output: Solve the equation 4x + y &gt; 5
```

These functions will output your string with the following values escaped:

| Char | Replacement |
|---|---|
| `&` | `&amp;` |
| `<` | `&lt;` |
| `>` | `&gt;` |
| `"` | `&quot;` |
| `\` | `&#39;` |
| `/` | `&#x2F;` |
| `` ` `` | `&#x60;` |
| `=` | `&#x3D` |

The stringify functions also accept other value types; `null` or `undefined`
won't appear in the output, arrays and other iterables
will be flattened (concatenated) by `encode`.

### Templates

Often you'll want to mix html and non-html values in the same line of code.
You can do this with the html template tag:

```javascript
import html from 'html-types';

const title = `The <h1> Tag`
const body = html`This is a <i>super</i> useful tag!`

// The title will be escaped because it's a string,
// but the body won't because we've created it with the html tag
const source = html`<article>
	<h1>${title}</h1>

	${body}
</article>`;

// The source is an HTML type, so the only thing that will be
// escaped will be the "<h1>" from the title.
console.log(html.encode(source));
```

The html tag creates a special data type that `encode` understands
as HTML content, which it won't escape. It will, however,
escape nested values using the same rules as above - including outputing
nested html templates verbatim.

### The class: Html

So what if you want to create this data type without using
a template? We provide a class for this. Html represents
a chunk of HTML data:

```javascript
import html, { Html, type HtmlTemplateVariable } from 'html-types';
import fs from 'fs';

const header = new Html(fs.readFileSync('./header.html'));

export default (content: HtmlTemplateVariable) => html`
<body>
	<header>
	${header}
	</header>
	<main>
	${content}
	</main>
`
```

### The protocol: implementing your own html objects

The Html class is just a simple implementation
of a protocols defined by this package. You can create your own
implementations of objects that can be represented as HTML by
implementing the protocol.

An object representing an html chunk must have the property
`value`, which is a getter that returns a string.
If you need to differentiate this from other objects that have
a `value` property, you may give the object a `type` property
with the string value `'text/html'`.


## Utilities

### `attrs`

Use this function to generate HTML element attributes. This function takes
a dict of key-value pairs and outputs an array of `Html` objects:

```javascript

const input = html`<input${html.attrs({
	name: 'enabled',
	value: '231',
	checked: true,
	readonly: false,
	class: null,
})}>`;
// Output: <input name="enabled" value="231" checked>

```