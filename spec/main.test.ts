import { expect, test } from '@jest/globals';
import html, { Html as HtmlChunk, attrs, isHtml } from '../src/main';

const stringify = html.encode;


test('Returns source as source', ()=>{
	const source = '<b>Html</b>';
	const chunk = new HtmlChunk(source);
	expect(chunk.toString()).toEqual(source);
	expect(chunk.type).toEqual('text/html');
});

test('Template fn return html chunk', ()=>{
	const chunk = html`<b>${'<3'}</b>`;
	expect(chunk.toString()).toEqual('<b>&lt;3</b>');
});

test('Compiler ignores nullish values', ()=>{
	expect(stringify(null)).toEqual('');
});

test('Compiler gets source from html chunk', ()=>{
	expect(stringify({ value: 'source' })).toEqual('source');
});

test('Compiler concats arrays', ()=>{
	expect(stringify([1,2,3])).toEqual('123');
});

test('Compiler throws other objects', ()=>{
	expect(stringify.bind(null, {} as any)).toThrow();
});

test('isHtml checks types', () => {
	expect(isHtml({ 'value': 'string' })).toBe(true);
	expect(isHtml({ 'value': 'string', type: 'text/plain' })).toBe(false);
	expect(isHtml('other')).toBe(false);
})

test('attrs generates attributes', () => {
	expect(attrs({ type: 'checkbox', checked: true, disabled: false, class: null })).toEqual([
		html` type="checkbox"`,
		html` checked`
	])
})